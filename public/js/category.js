jQuery(function()
{
	function showSpinner()
	{
		jQuery('.spin-container').removeClass('d-none');
	}
	function hideSpinner()
	{
		jQuery('.spin-container').addClass('d-none');
	}
	function getBlogPosts(ids)
	{
	    let tab = document.querySelector('#blog-posts');
	    let endpoint = 'https://blog.comparasoftware.com/wp-json/wp/v2/posts?include=' + ids;
	    tab.innerHTML = '';
	    showSpinner();
	    jQuery.get(endpoint, (res) =>
	    {
	    	hideSpinner();
	        for(let post of res)
	        {
	            let container 		= document.createElement('div'); 
	            let row 			= document.createElement('div');
	            let postContainer   = document.createElement('div');
	            let imageContainer  = document.createElement('div');
	            let dataContainer   = document.createElement('div');
	            postContainer.className = 'blog-post-container';
	            container.className = 'container';
	            row.className = 'row';
	            imageContainer.className = 'col-12 col-sm-5 image-container';
	            dataContainer.className = 'col-12 col-sm-7 data-container';
	            let anchor          = document.createElement('a');
	            let figure          = document.createElement('figure');
	            let image           = document.createElement('img');
	            let title           = document.createElement('h3');
	            let titleLink       = document.createElement('a');
	            let excerpt         = document.createElement('div');
	            let btn             = document.createElement('a');
	            anchor.href         = post.link;
	            image.src           = post.image;
	            image.className		= 'img-fluid';
	            titleLink.href      = post.link;
	            titleLink.innerHTML = post.title.rendered;
	            excerpt.className   = 'excerpt';
	            excerpt.innerHTML   = post.excerpt.rendered;
	            btn.target          = '_blank';
	            btn.href            = post.link;
	            btn.innerHTML       = 'Leer m&aacute;s';
	            btn.className       = 'btn btn-green';
	            title.appendChild(titleLink);
	            figure.appendChild(image);
	            anchor.appendChild(figure);
	            imageContainer.appendChild(anchor);
	            dataContainer.appendChild(title);
	            dataContainer.appendChild(excerpt);
	            dataContainer.appendChild(btn);
	            row.appendChild(imageContainer);
	            row.appendChild(dataContainer);
	            container.appendChild(row);
	            postContainer.appendChild(container);
	            tab.appendChild(postContainer);
	        }
	    });
	}
	jQuery('#btn-tab-blog').click(function()
	{
	    if( !this.dataset.blogcid )
	    {
	        jQuery('#tab-blog #no-posts').removeClass('d-none');
	        return true;
	    }
	    getBlogPosts(this.dataset.blogcid);
	});
	jQuery('#btn-tab-comments').click(function()
	{
		jQuery('#comments-list').html('');
		showSpinner();
		jQuery.get(this.dataset.endpoint, function(res)
		{
			hideSpinner();
			jQuery('#comments-list').html(res.html);
		});
		return true;
	});
});