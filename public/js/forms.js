jQuery(function()
{
	//document.addEventListener('formbuilder.step.changed');
	function nextStep(carousel, modal, steps)
	{
		let total_steps 		= steps.length;
		//##get current step
		let currentStep 		= carousel.find('.carousel-item.active');
		let currentStepIndex 	= steps.indexOf(currentStep.get(0));
		console.log(currentStep, currentStepIndex);
		carousel.carousel('next');
		let eventChangedStep = new CustomEvent('formbuilder.step.changed', {detail: null});
		document.dispatchEvent(eventChangedStep);
		let nextIndex = currentStepIndex + 1;
		if( (nextIndex + 1) == total_steps )
		{
			//##show last step buttons
			modal.find('.step-buttons').addClass('d-none');
			modal.find('.modal-form-buttons').removeClass('d-none');
		}
	}
	function checkLastStep(carousel, steps)
	{
		let total_steps 		= steps.length;
		//##get current step
		let currentStep 		= carousel.find('.carousel-item.active');
		let currentStepIndex 	= steps.indexOf(currentStep.get(0));
		if( currentStepIndex == (total_steps - 1) )
			return true;
		return false;
	}
	function findProgress(carousel)
	{
		let currentStep = carousel.find('.carousel-item.active');
		let progress	= currentStep.find('.cprogress');
		if( !progress || progress.length <= 0 )
			return false;
		
		setTimeout(function(){progress.find('.progress-value').html('Validando');}, 4000);
		setTimeout(function(){carousel.carousel('next');}, 4500);
	}
	function validateElements(container)
	{
		let error = [];
		for(let ctrl of container.querySelectorAll(':required'))
		{
			ctrl.classList.remove('is-invalid');
			if( !ctrl.validity.valid )
			{
				ctrl.classList.add('is-invalid');
				error.push( {error: '', ctrl: ctrl} );
				console.log(ctrl);
				//break;
			}
		}
		if( error && error.length > 0)
			throw error;
		return true;
	}
	function showModalSubmit(modal)
	{
		modal.querySelector('.modal-actions').style.display = 'none';
		modal.querySelector('.modal-footer').style.display = 'flex';
	}
	function openForm(form)
	{
		let modal = jQuery('#modal-form-' + form.id);
		if( modal.length > 0 )
			modal.remove();
		let htmlForm = atob(form.buffer);
		jQuery(document.body).append(htmlForm);
		modal = jQuery('#modal-form-' + form.id);
		let $form = modal.find('form:first');
		modal.modal('show');
		let carousel = modal.find('.carousel:first');
		let is_carousel = carousel.length > 0 ? true : false;
		let steps = [];
		if( is_carousel )
		{
			steps = Array.from(modal.get(0).querySelectorAll('.carousel-item'));
			carousel.on('slid.bs.carousel', function()
			{
				findProgress(carousel);
				if( checkLastStep(carousel, steps) )
					showModalSubmit(modal.get(0));
			});
			jQuery('.btn-next', modal).click(function()
			{
				nextStep(carousel, modal, steps);
			});
			jQuery('.btn-next-step', modal).click(function(e)
			{
				try
				{
					//validateElements(this.closest('.carousel-item'));
					validateElements(modal.get(0).querySelector('.carousel-item.active'));
					nextStep(carousel, modal, steps);
				}
				catch(e)
				{
					console.log(e);
					if( e.ctrl )
						e.ctrl.classList.add('is-invalid');
				}
				return true;
			});
		}
		//##set submit event
		if( $form.hasClass('form-ajax') )
		{
			$form.submit(function(e)
			{
				e.preventDefault();
				try
				{
					validateElements($form.get(0));
					submitForm($form.get(0))
						.then( (res) => 
						{
							if( is_carousel )
							{
								let messageStep = jQuery('<div class="carousel-item form-message-step" />');
								messageStep.html(buildMessage(res.message));
								carousel.find('.carousel-inner').append(messageStep);
								steps = Array.from(modal.get(0).querySelectorAll('.carousel-item'));
								nextStep(carousel, modal, steps);
								modal.find('.modal-form-buttons .btn-success').addClass('d-none');
							}
							else
							{
								modal.find('.modal-body').html(buildMessage(res.message));
								modal.find('.modal-footer').html('');
							}
							let evt = new CustomEvent('cs_form_submit', {
								detail: {
									form: $form.get(0), 
									modal: modal,
									carousel: is_carousel,
									response: res
								}
							});
							document.dispatchEvent(evt);
						})
						.catch( (e) => 
						{
							let evt = new CustomEvent('cs_form_submit_error');
							document.dispatchEvent(evt);
						});
				}
				catch(e)
				{
					if( e.ctrl )
						e.ctrl.classList.add('is-invalid');
				}
				
				return false;
			});
		}
	}
	function loadForm(id, args, code)
	{
		let endpoint = jg.baseurl + '/formbuilder/';
		if( code )
			endpoint += 'ff/';
		endpoint += id;
		
		jQuery.get(endpoint + '?'+ args.join('&') + '&t=' + (new Date()).getTime(), function(res)
		{
			openForm(res);
		})
		.fail( (e) => 
		{
			console.log(e);
		});
	}
	function submitForm($form)
	{
		return new Promise( (resolve, reject) => 
		{
			let btn = $form.querySelector('button[type=submit]');
			btn ? btn.style.display = 'none' : '';
			try
			{
				let params = jQuery($form).serialize();
				jQuery.post($form.action, params, function(res)
				{
					btn ? btn.style.display = 'inline' : '';
					resolve(res);
				})
				.fail(function(e)
				{
					btn ? btn.style.display = 'inline' : '';
					console.log(e);
					if( e.responseJSON && e.responseJSON.error )
					{
						alert(e.responseJSON.error);
					}
					reject(e);
				});
			}
			catch(e)
			{
				btn ? btn.style.display = 'inline' : '';
				console.log(e);
			}
		});
	}
	function buildMessage(message)
	{
		return '<p class="alert alert-success text-center alert-form-success">'+ 
					message +'<br><i class="fa fa-check-circle"></i></p>'
	}
	jQuery('.static-form.form-ajax').submit(function(e)
	{
		e.preventDefault();
		let spinner = Utils.buildSpinner('Processing...');
		this.appendChild(spinner);
		submitForm(this).then( (res) => 
		{
			this.parentNode.innerHTML = buildMessage(res.message);
			spinner.remove();
		})
		.catch( () => 
		{
			spinner.remove();
		});
		return false;
	});
	jQuery('.btn-form').click(function(e)
	{
		e.preventDefault();
		let args = [];
		
		for(let key in this.dataset)
		{
			if( key.indexOf('param_') == -1 )
				continue;
			let param = key.split('_')[1];
			let value = this.dataset[key];
			args.push('data['+ param +']=' + value);
		}
		if( this.dataset.form_id )
		{
			loadForm(this.dataset.form_id, args);
		}
		else if( this.dataset.code )
		{
			loadForm(this.dataset.code, args, true);
		}
		return false;
	});
	//##phone validation events
	jQuery(document).on('focus', '.telephone-control .phone-control', function(e)
	{
		this.classList.remove('is-invalid');
	});
	jQuery(document).on('blur', '.telephone-control .phone-control', function(e)
	{
		if( this.dataset.avoid && this.dataset.avoid == 'true' )
			return true;
		let $input 			= this;
		let container		= this.closest('.telephone-control');
		let calling_code 	= container.querySelector('[name=calling_code]').value;
		let number			= calling_code + this.value;
		if( number.trim().length <= 0 )
			number = 000;
		
		jQuery.get(jg.route_verify_number + '/' +  number, function(res)
		{
			
		})
		.fail( (e) => 
		{
			console.log('FAILED', e);
			$input.parentNode.querySelector('.invalid-feedback').innerHTML = e.responseJSON.error;
			$input.classList.add('is-invalid');
		});
	});
	jQuery(document).on('click', '.avoid-phone-validation', function(e)
	{
		let ctrl = this.closest('.telephone-control').querySelector('.phone-control');
		ctrl.dataset.avoid = 'true';
		ctrl.classList.remove('is-invalid');
		return false;
	});
	jQuery(document).on('keydown', '[name=calling_code], [name=phone], [name=ext]', function(e)
	{
		if( e.keyCode == 8 || e.keyCode == 9 )
			return true;
		if( !(e.keyCode >= 48 && e.keyCode <= 57) )
			return false;
		return true;
	});
});