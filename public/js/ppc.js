(function()
{
	class Ppc
	{
		constructor()
		{
			
		}
		Init()
		{
			document.querySelectorAll('.ppc-provider-site').forEach((btn) => 
			{
				let link = btn.href || btn.dataset.href;
				if( link )
				{
					btn.addEventListener('click', (e) => 
					{
						this.Track(e.target);
					});
				}
			});
		}
		Track(btn)
		{
			console.log(btn);
			let data = {
				sid: btn.dataset.sid || 0,
				cid: btn.dataset.cid || 0,
				source: btn.dataset.source || 'btn',
				target_url: btn.href || btn.dataset.href || 'not defined'
			};
			let headers = {'Content-type': 'application/json', 'X-CSRF-TOKEN': jg.csrf_token};
			fetch(jg.baseurl + '/ppc/track', {method: 'POST', body: JSON.stringify(data), credentials: 'include', headers: headers});
		}
	}
	let ppc = new Ppc();
	ppc.Init();
	
})();