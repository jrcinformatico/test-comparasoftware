class Comparator
{
	constructor()
	{
		this.items = [];
	}
	AddItem(item)
	{
		if( this.HasItem(item) != null )
			return false;
		this.items.push(item);
		this.Save();
		this.LoadWidget();
		return true;
	}
	RemoveItem(item)
	{
		let $exists = this.HasItem(item);
		if( $exists == null )
			return false;
		this.items.splice($exists, 1);
		this.Save();
		this.LoadWidget();
		return true;
	}
	HasItem(item)
	{
		let index = null;
		for(let i = 0; i < this.items.length; i++)
		{
			let $item = this.items[i];
			if( $item.id == item.id )
			{
				index = i;
				break;
			}
		}
		return index;
	}
	GetIdItems()
	{
		let ids = [];
		this.items.map( (v, i) => ids.push(v.id) );
		
		return ids;
	}
	Init()
	{
		this.Load();
		this.CheckCurrentItems();
		document.addEventListener('click', (e) => 
		{
			if( e.target.classList.contains('add-2-compare') )
			{
				e.preventDefault();
				e.stopPropagation();
				if(this.items.length >=4 )
					return false;
				console.log(e.target.dataset);
				if( !e.target.dataset )
					return false;
				if( !e.target.dataset.id || !e.target.dataset.label || !e.target.dataset.image )
					return false;
				let item = {
					id: e.target.dataset.id,
					label: e.target.dataset.label,
					image: e.target.dataset.image,
					link: e.target.dataset.href || 'javascript:;'
				};
				let exists = this.HasItem(item);
				console.log('Exists: ', exists);
				if( exists == null )
				{
					this.AddItem(item);
					e.target.classList.add('in-compare');
					if( e.target.dataset['in'] )
						e.target.innerHTML = e.target.dataset['in']
				}
				else
				{
					this.RemoveItem(item);
					e.target.classList.remove('in-compare');
					if( e.target.dataset['out'] )
						e.target.innerHTML = e.target.dataset['out']
				}
				
				return false;
			}
		});
		document.querySelector('#comparator-bar .btn-close-comparator').addEventListener('click', (e) => {this.HideBar();});
		document.querySelector('#btn-view-comparator').addEventListener('click', (e) => 
		{
			e.preventDefault();
			let url = e.target.href || e.target.parentElement.href;
			url += '/?compareids=' + this.GetIdItems().join(',')
			window.location = url;
		}, false);
	}
	Save()
	{
		let json = JSON.stringify(this.items);
		sessionStorage.setItem('comparator', json);
	}
	Load()
	{
		let json = sessionStorage.getItem('comparator');
		if( !json )
			return false;
		this.items = JSON.parse(json);
		this.LoadWidget();
	}
	CheckCurrentItems()
	{
		document.querySelectorAll('.list-item-new .add-2-compare').forEach((el) => 
		{
			if( this.HasItem({id: el.dataset.id}) != null )
			{
				el.classList.add('in-compare');
				if( el.dataset['in'] )
					el.innerHTML = el.dataset['in']
			}
		});
	}
	ShowBar(withOverlay)
	{
		if( withOverlay )
		{
			let overlay = document.querySelector('#comparator-overlay');
			if( !overlay )
			{
				let overlay = document.createElement('div');
				overlay.setAttribute('id', 'comparator-overlay');
				document.body.appendChild(overlay);
			}
		}
		let compare_bar = document.querySelector('#comparator-bar');
		compare_bar.classList.add('show');
	}
	HideBar()
	{
		let overlay = document.querySelector('#comparator-overlay')
		if( overlay )
			overlay.remove();
		let compare_bar = document.querySelector('#comparator-bar');
		compare_bar.classList.remove('show');
	}
	LoadWidget()
	{
		if( this.items.length <= 0 )
			return false;
		/*
		let widget = document.querySelector('#comparator-widget');
		if( !widget )
		{
			widget = document.createElement('a');
			widget.setAttribute('id', 'comparator-widget');
			widget.innerHTML = '<i class="fa fa-balance-scale"></i><span id="items-count" class="badge badge-light"></span></a>';
			widget.href = "javascript:;"
			widget.addEventListener('click', (e) => this.ShowBar(e));
			document.body.appendChild(widget);
		}
		widget.querySelector('#items-count').innerHTML = this.items.length;
		*/
		let compare_bar = document.querySelector('#comparator-bar');
		if( !compare_bar )
			return false;
		let compare_items = compare_bar.querySelector('#comparator-items .row');
		compare_items.innerHTML = '';
		let i = 0;
		for(let item of this.items)
		{
			let $item = document.createElement('div');
			$item.className = 'comparator-item col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2';
			$item.dataset.index = i;
			let $item_img = document.createElement('a');
			$item_img.setAttribute('href', item.link);
			$item_img.innerHTML = '<img src="'+ item.image +'" alt="'+ item.label +'" class="img-fluid img-thumbnail" />';
			//let $item_label = document.createElement('a');
			//$item_label.setAttribute('href', item.link);
			//$item_label.innerHTML = item.label;
			let btn_close = document.createElement('a');
			btn_close.setAttribute('href', 'javascript:;');
			btn_close.className = "btn-remove-item";
			btn_close.innerHTML = '<i class="fa fa-times-circle"></i>';
			btn_close.addEventListener('click', (e) => 
			{
				if( !this.RemoveItem(item) )
					return false;
				$item.remove();
				return false;
			});
			$item.appendChild($item_img);
			$item.appendChild(btn_close);
			compare_items.appendChild($item);
			i++;
		}
		this.ShowBar();
	}
}
class Utils
{
	static buildSpinner(text)
	{
		let div = document.createElement('div');
		div.className = 'spin-container text-center';
		let i = document.createElement('i');
		i.className = 'fa fa-spinner fa-spin fa-3x fa-fw';
		if( text )
		{
			let tdiv = document.createElement('div');
			tdiv.innerHTML = text;
			div.appendChild(tdiv);
		}
		div.appendChild(i);
		
		return div;
	}
	static lazyLoader()
	{
		let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

		if ("IntersectionObserver" in window) 
		{
		    let lazyImageObserver = new IntersectionObserver(function(entries, observer) 
		    {
		    	entries.forEach(function(entry) 
		    	{
			        if (entry.isIntersecting) 
			        {
			        	let lazyImage = entry.target;
			        	lazyImage.onload = () => {lazyImage.classList.remove("lazy");}
			        	lazyImage.src = lazyImage.dataset.src.replace('https:', '').replace('http:', '');
			        	if( lazyImage.dataset.srcset )
			        		lazyImage.srcset = lazyImage.dataset.srcset;
			        	
			        	lazyImageObserver.unobserve(lazyImage);
			        }
		    	});
		    });
		    lazyImages.forEach(function(lazyImage) {
		    	lazyImageObserver.observe(lazyImage);
		    });
		} 
		else 
		{
			// Possibly fall back to a more compatible method here
		}
	}
}
jQuery(function()
{
	window.comparator = new Comparator();
	comparator.Init();
	if( document.querySelector('#home-top') )
		document.querySelector('#home-top').style.height = window.innerHeight + 'px';
	Utils.lazyLoader();
	jQuery('#btn-mobile-menu').click(function(e)
	{
		e.preventDefault();
		e.stopPropagation();
		jQuery('#navigation').addClass('open');
	});
	jQuery(document).click(function(e)
	{
		/*
		if( jQuery(e.target).hasClass('menu-item') )
			return true;*/
		if( jQuery('#navigation').hasClass('open') )
			jQuery('#navigation').removeClass('open');
		return true;
	});
	//##get customer geodata
	// if( !sessionStorage.getItem('geodata') )
	// {
	// 	jQuery.get('/geodata', function(res)
	// 	{
	// 		let geodata = {
	// 			country_code: res.country_code,
	// 			country_name: res.country_name,
	// 			city: res.city,
	// 			ip: res.ip
	// 		}
	// 		sessionStorage.setItem('geodata', JSON.stringify(geodata));
	// 		let evt = new CustomEvent('geodata', {detail: geodata});
	// 		document.dispatchEvent(evt);
	// 	});
	// }
	// else
	// {
		
	// }
});
window.onload = function()
{
	for(let $if of document.querySelectorAll('iframe[data-src]'))
	{
		$if.src = $if.dataset.src;
	}
};