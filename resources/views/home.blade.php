@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 mb-4">
            <button type="button" class="btn btn-primary float-right">Add new Software</button>
        </div>
    </div>

    <div class="row">
    <div class="col-lg-12">
        <div class="card">
            {{--  <div class="card-header">
                <i class="fa fa-align-justify"></i> List
                <button type="button" class="btn btn-primary float-right">Add Software</button>
            </div>  --}}
            <header class="card-header">
                <strong>Listing </strong> <small>softwares</small>
            </header>
            
            <div class="card-body">
                <table class="table table-responsive-sm">
                <thead>
                    <tr>
                    <th>Username</th>
                    <th>Date registered</th>
                    <th>Role</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Samppa Nori</td>
                    <td>2012/01/01</td>
                    <td>Member</td>
                    <td><span class="badge badge-success">Active</span></td>
                    </tr>
                    <tr>
                    <td>Estavan Lykos</td>
                    <td>2012/02/01</td>
                    <td>Staff</td>
                    <td><span class="badge badge-danger">Banned</span></td>
                    </tr>
                </tbody>
                </table>
                <!--  <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>  -->
            </div>  
          
        </div>

    </div>
    <!-- /.col-->
    </div>
    <!-- /.row-->

    <div class="row">
    <div class="col-lg-12">

        <div class="card">
            <header class="card-header">
                <strong>Add new </strong> <small>software</small>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div role="group" class="form-group">
                        <label for="uid-lmr5ga7wdvh" class=""> Title </label><!---->
                        <input id="uid-lmr5ga7wdvh" type="text" placeholder="Enter your name" class="form-control"><!----><!----><!----><!---->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                    <div role="group" class="form-group form-row">
                        <label for="uid-8at8fljwje" class="col-form-label col-sm-3"> Description </label>
                        <div class="col-sm-9">

                            <textarea id="uid-8at8fljwje" placeholder="Content..." rows="9" class="form-control"></textarea>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div role="group" class="form-group">
                        <label for="uid-hmjbjt4hv1c" class=""> URL image </label><!---->
                        <input id="uid-hmjbjt4hv1c" type="text" placeholder="https://cdn.pixabay.com/photo/image.jpg" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div role="group" class="form-group">
                        <label for="uid-hmjbjt4hv1c" class=""> URL Software </label><!---->
                        <input id="uid-hmjbjt4hv1c" type="text" placeholder="https://coreui.io" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">

                        <div class="form-row form-group">
                        <label class="col-form-label col-sm-3"> Languaje </label>
                        <div class="col-sm-9">
                            <div role="group" class="custom-control custom-checkbox">
                                <!----><input id="uid-esjsbvd5epj" type="checkbox" name="Option 12" class="custom-control-input" value="Option 1">
                                <label for="uid-esjsbvd5epj" class="custom-control-label"> Option 1 </label><!----><!----><!----><!---->
                            </div>
                            <div role="group" class="custom-control custom-checkbox">
                                <!----><input id="uid-wa0oyydvk1" type="checkbox" name="Option 12" class="custom-control-input" value="Option 2">
                                <label for="uid-wa0oyydvk1" class="custom-control-label"> Option 2 </label><!----><!----><!----><!---->
                            </div>
                            <div role="group" class="custom-control custom-checkbox">
                                <!----><input id="uid-42oyxsuz4lp" type="checkbox" name="Option 12" class="custom-control-input" value="Option 3">
                                <label for="uid-42oyxsuz4lp" class="custom-control-label"> Option 3 </label><!----><!----><!----><!---->
                            </div>
                        </div>
                        </div>


                    </div>
                </div>


            </div>
            <footer class="card-footer ">
                <div class="float-right">
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    <button type="reset" class="btn btn-light btn-sm"> Cancel</button>
                </div>
            </footer>
        </div>
    </div>
    <!-- /.col-->
    </div>
    <!-- /.row-->
@endsection

{{--  @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
            <example-component>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection  --}}
