@extends('landing.layout')

@section('content')
    <div id="layoutDefault_content">
    <main>
        <nav class="navbar navbar-marketing navbar-expand-lg bg-transparent navbar-dark">
            <div class="container-fluid">
                <a href="{{ url('/') }}">
                <img src="http://dev.comparasoftware.com/platform/images/logo-white.png" alt="" class="img-fluid nolazy" style="max-width:300px" />
                </a>   
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mr-lg-5">
                            <li class="nav-item">
                                <a class="nav-link" href="/dashboard">Ir a dashboard </a>
                            </li>
                        </ul>
                    </div>
                </div
            </div>
        </nav>
        <header class="page-header page-header-dark bg-gradient-primary-to-secondary" style="padding-bottom:4rem;">
            <div class="page-header-content">
                <div class="container-fluid">
                <div class="row mx-lg-5 no-gutters">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                        <h1 class="page-header-title">
                            <h1 class="page-header-title">Software de Mantenimiento</h1>
                        </h1>
                        <p style="font-size:20px;">
                            Cuando se habla del software de gestión de mantenimiento es común utilizar el término CMMS. Se trata del acrónimo de Computerized Maintenance Management Software-System. Asimismo, para referirse a este tipo de programa informático también suele... 
                            <a href="#category-content" onclick="jQuery('#category-tabs ul li:last-child a').tab('show');" style="color:#fff;">Leer m&aacute;s</a>
                        </p>
                        <p class="mb-2 mt-5" style="font-size:1.5rem;">¿Utilizas actualmente un Software de Mantenimiento?</p>
                    </div>
                </div>
                <div class="row mx-lg-5 no-gutters">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                        <div>
                            <a class="btn btn-yes rounded-pill btn-form btn-header-yes btn-primary mb-3" href="#!"  style="margin-left:0;">Sí</a>
                            <a class="btn rounded-pill btn-no btn-form btn-header-no mb-3">No</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="svg-border-waves text-light">
                <svg class="wave" style="pointer-events: none" fill="currentColor" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 75">
                <defs>
                    <style>
                        .a {fill: none;}
                        .b {clip-path: url(#a);}
                        .d {opacity: 0.5;isolation: isolate;}
                    </style>
                    <clipPath id="a">
                        <rect class="a" width="1920" height="75"></rect>
                    </clipPath>
                </defs>
                <title>wave</title>
                <g class="b">
                    <path class="c" d="M1963,327H-105V65A2647.49,2647.49,0,0,1,431,19c217.7,3.5,239.6,30.8,470,36,297.3,6.7,367.5-36.2,642-28a2511.41,2511.41,0,0,1,420,48"></path>
                </g>
                <g class="b">
                    <path class="d" d="M-127,404H1963V44c-140.1-28-343.3-46.7-566,22-75.5,23.3-118.5,45.9-162,64-48.6,20.2-404.7,128-784,0C355.2,97.7,341.6,78.3,235,50,86.6,10.6-41.8,6.9-127,10"></path>
                </g>
                <g class="b">
                    <path class="d" d="M1979,462-155,446V106C251.8,20.2,576.6,15.9,805,30c167.4,10.3,322.3,32.9,680,56,207,13.4,378,20.3,494,24"></path>
                </g>
                <g class="b">
                    <path class="d" d="M1998,484H-243V100c445.8,26.8,794.2-4.1,1035-39,141-20.4,231.1-40.1,378-45,349.6-11.6,636.7,73.8,828,150"></path>
                </g>
                </svg>
            </div>
        </header>
        <div id="category-tabs">
            <div class="container-fluid">
                <div class="row mx-lg-5 no-gutters">
                <div class="col">
                    <div>
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                            <a href="#category-listing" class="nav-link active" data-toggle="tab">Listado de software</a>
                            </li>
                            <li class="nav-item">
                            <a href="#category-content" class="nav-link" data-toggle="tab">
                            ¿Qué es un Software de Mantenimiento?
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row mx-lg-5 no-gutters">
                <div class="col">
                <div class="tab-content">
                    <div id="category-content" class="tab-pane">
                        <div class="container-fluid">
                            <div class="row justify-content-md-center">
                            <div class="col-xs-12 col-sm-8">
                                <h1>Software de Mantenimiento</h1>
                                <div id="">
                                    <p>Software de mantenimiento &hellip; Cuando se habla del software de gesti&oacute;n de mantenimiento es com&uacute;n utilizar el t&eacute;rmino CMMS. Se trata del acr&oacute;nimo de <em>Computerized Maintenance Management Software-System</em>. Asimismo, para referirse a este tipo de programa inform&aacute;tico tambi&eacute;n suele hacerse uso de otros dos acr&oacute;nimos m&aacute;s. Puede utilizarse GMAO, por Gesti&oacute;n del Mantenimiento Asistido por Ordenador. Tambi&eacute;n se las siglas GMAC, por Gesti&oacute;n del Mantenimiento Asistido por Computador. En general, todas esas denominaciones coinciden en identificar a una herramienta que sirve para asumir el control del estado de los equipos e instalaciones de la empresa a fin de que funcionen cabalmente. Esto abarca el mantenimiento correctivo y preventivo, entre otros.</p>
                                    <h3><strong>Software de gesti&oacute;n de mantenimiento para preservar el patrimonio de la empresa</strong></h3>
                                    <p>Por su evoluci&oacute;n en el tiempo, ya no tiene solo como foco principal el objetivo que le da nombre. Ahora, tambi&eacute;n el software de gesti&oacute;n de mantenimiento puede ofrecer funciones adicionales que lo hacen ser a&uacute;n m&aacute;s eficaz. Sin importar sus dimensiones, el sector de la actividad econ&oacute;mica que las enmarque ni el producto o servicio que ofrezcan, todas las organizaciones pueden adquirir un software como este, que les resulte eficiente y que se ajuste a sus requerimientos.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    <div id="category-listing" class="tab-pane active">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 order-md-2 order-lg-1">

                            @include('landing.filter')

                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 order-md-1 order-lg-2">
                            <div class="row mb-3" style="font-size:20px;">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div >
                                        <b style="color:#000;">{{count($products)}}</b> apps en total
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="text-right">
                                        Ordenar por:&nbsp;&nbsp;
                                        <select name="" class="d-inline">
                                        <option>Relevancia</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            @include('landing.softwareList')

                            </div>
                        </div>
                    </div>


                </div>
                </div>
            </div>
        </div>

        <section class="category-bottom py-10">
            <div class="container">
                <div class="row justify-content-center">
                <div class="col-lg-11">
                    <div class="wrap-content">
                        <div class="container-fluid">
                            <div class="row">
                            <div class="col-lg-7">
                                <h2 class="display-4 text-dark font-weight-800">¿Encontraste lo que buscabas?</h2>
                                <p class="lead text-dark-50 mb-5">Si necesitas ayuda con tu búsqueda, no dudes en solicitar una asesoría.</p>
                            </div>
                            <div class="col-lg-5">
                                <div class="text-right">
                                    <div class="mt-4"><a href="#" class="btn btn-primary" disabl>Necesito ayuda</a></div>
                                    <span class="text-dark-50">Puedes solicitar contacto en cualquier momento</span>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="svg-border-rounded text-dark">
                <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor">
                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
                </svg>
            </div>
        </section>
    </main>
    </div>
@endsection