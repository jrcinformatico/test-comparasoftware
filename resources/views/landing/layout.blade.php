<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <link rel="icon" type="image/x-icon" href="{{URL::asset('images/favicon-150x150.png')}}" />
      <title>
        @section('title')
        ▷ Compara los Mejores Software De Mantenimiento【2020】
        @show
        </title>
      <link href="{{URL::asset('css/styles.css')}}" rel="stylesheet" />
      <!-- <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> -->
      <link href="{{URL::asset('css/css-style.css')}}" rel="stylesheet" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
   </head>

   <body class="new-category category category-140 mid-4457 ">
      <div id="layoutDefault">

         @yield('content')

         <div id="layoutDefault_footer">
            <footer class="footer pt-10 pb-5 mt-auto bg-light footer-light bg-dark">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-3">
                        <div class="footer-brand">ComparaSoftware SPA</div>
                        <div class="mb-3">Ayudamos a empresas a tomar decisiones informadas sobre la elección de sus herramientas digitales.</div>
                        <div class="icon-list-social mb-5">
                           <a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.instagram.com/compara.software/">
                           <i class="fa fa-instagram"></i>
                           </a>
                           <a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.facebook.com/comparasoftware/?_rdc=2&_rdr">
                           <i class="fa fa-facebook"></i>
                           </a>
                           <a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.linkedin.com/company/11402374/admin/">
                           <i class="fa fa-linkedin"></i>
                           </a>
                           <a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://twitter.com/ComparaSoftware">
                           <i class="fa fa-twitter"></i>
                           </a>
                        </div>
                     </div>
                     <div class="col-lg-9">
                        <div class="row">
                           <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                              <div class="text-uppercase-expanded text-xs mb-4">Nuestra empresa</div>
                              <ul class="list-unstyled mb-0">
                                 <li class="mb-2"><a href="/nuestra-empresa">Sobre Nosotros</a></li>
                                 <li class="mb-2"><a href="https://blog.comparasoftware.com" target="blank">Blog</a></li>
                                 <li class="mb-2"><a href="javascript:void(0);">Sitemap</a></li>
                                 <li class="mb-2"><a href="https://contrataciones.comparasoftware.com/careers" rel="nofollow">Trabaja con nosotros</a></li>
                              </ul>
                           </div>
                           <div class="col-lg-4 col-md-6 mb-5 mb-md-0">
                              <div class="text-uppercase-expanded text-xs mb-4">Empresas de Software</div>
                              <ul class="list-unstyled mb-0">
                                 <li class="mb-2"><a href="#">Nuestros Servicios</a></li>
                                 <li class="mb-2"><a href="#">Registrar un software</a></li>
                                 <li class="mb-2"><a href="#">Iniciar sesion</a></li>
                              </ul>
                           </div>
                           <div class="col-lg-4 col-md-6">
                              <div class="text-uppercase-expanded text-xs mb-4">Contáctanos</div>
                              <div class="mb-3">
                                 <p>info@comparasoftware.com</p>
                                 <p style="font-size: 13px;line-height: 0;">Chile +56-2-2958-9840</p>
                                 <p style="font-size: 13px;line-height: 0;">Argentina: +54-11-5279-8391</p>
                                 <p style="font-size: 13px;line-height: 0;">Colombia: +57-1-58023110</p>
                                 <p style="font-size: 13px;line-height: 0;">México: +52-55-8526-5801</p>
                                 <p style="font-size: 13px;line-height: 0;">Perú: +51-1-6429811</p>
                                 <br>
                                 <p style="font-size: 13px;line-height: 0;">Perez Valenzuela, Piso 10</p>
                                 <p style="font-size: 13px;line-height: 0;">Santiago -Chile </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <hr class="my-5" />
                  <div class="row align-items-center">
                     <div class="col-md-6 small">ComparaSoftware SPA 2020</div>
                     <div class="col-md-6 text-md-right small">
                        <a href="#">Políticas de Privacidad</a>
                        &middot;
                        <a href="#">Políticas de Cookies</a>
                        <a href="#">Términos y Condiciones de uso</a>
                     </div>
                  </div>
               </div>
            </footer>
         </div>
         <!-- end id="layoutDefault_footer" -->

         <div id="comparator-bar">
            <div class="container-fluid">
               <div class="row no-gutters">
                  <div class="col-10 col-sm-7">
                     <div id="comparator-items" class="container-fluid"><div class="row no-gutters"></div></div>
                  </div>
                  <div class="col-2 col-sm-5">
                     <div class="mt-3">
                        <a href="http://dev.comparasoftware.com/platform/comparador" 
                           id="btn-view-comparator" class="btn btn-secondary">
                           <i class="fa fa-check"></i>
                           <span class="text d-none d-sm-inline">Ver comparación</span>
                        </a>
                        <a href="javascript:;" class="btn-close-comparator btn">
                           <i class="fa fa-close"></i>
                           <span class="text d-none d-sm-inline">Cerrar comparador</span>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            
      </div>


      <script src="{{ asset('js/home.js')}}"></script>

      <!-- <script>
         let jg = {"baseurl":"http:\/\/dev.comparasoftware.com\/platform","csrf_token":"2l9DjMR7MNDrbX6tpyHaKTom9uU8NQa0AgjTPIiw","route_verify_number":"http:\/\/dev.comparasoftware.com\/platform\/formbuilder\/verifynumber"};
      </script> -->

      <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
      <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
      <script>
         AOS.init({disable: 'mobile', duration: 600, once: true});
      </script>

      <script src="{{ asset('js/scripts.js')}}"></script>
      <script src="{{ asset('js/front.js')}}"></script>
      <script src="{{ asset('js/category.js')}}"></script>

      <script src="{{ asset('js/forms.js')}}"></script>
      <script src="{{ asset('js/ppc.js')}}"></script>


   </body>
</html>