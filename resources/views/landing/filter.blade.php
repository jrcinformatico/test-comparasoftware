<aside id="cartegory-sidebar-listing" class="sidebar">
    <div class="widget">
        <div id="filter-card" class="card">
            <div class="card-header">
            <h5 class="card-title">Filtros de Búsqueda</h5>
            </div>

            @foreach ($categories as  $category)
            <ul class="feature-box">
            <li>
                <a href="#feature-childs-{{$category->id}}" data-toggle="collapse">
                <i class="fa lenguage"></i> {{$category->name}}
                </a>
                <ul id="feature-childs-{{$category->id}}" class="feature-items collapse multi-collapse ">
                    @foreach ($category->features as $feature)
                    <li>
                        <div class="custom-control custom-checkbox custom-control-solid filter">
                        <input class="custom-control-input filter-value" id="filter-cb-{{$feature->id}}" type="checkbox" name="" value="{{$feature->id}}" />
                        <label class="custom-control-label filter" for="filter-cb-{{$feature->id}}">{{$feature->name}}</label>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </li>
            </ul>
            @endforeach

        </div>
    </div>
</aside>