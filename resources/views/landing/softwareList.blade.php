<ul id="software-list" class="list" itemscope itemtype="https://schema.org/ItemList">
    <meta itemprop="numberOfItems" content="" />

    @foreach ($products as $product)

    <li class="list-item-new software-4520" data-mid="41277" 
        itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" data-sf='[@foreach ($product->productFeatures()->select('feature_id')->get() as $i => $row )  @if ($i==0) "{{$row->feature_id}}" @endif,"{{$row->feature_id}}"
    @endforeach]' data-cf='[]'>
        <meta itemprop="position" content="1" />
        <span class="new-verified-badge">
        <img src="http://dev.comparasoftware.com/platform/images/verificado-green.png" alt="{{$product->title}}" />
        </span>
        <div class="row no-gutters">

            <div class="col-xs-12 col-sm-3">
            <div id="software-image-4520" class="software-image">
                <a href="http://dev.comparasoftware.com/platform/cmms-mp-version-10">
                <img alt="{{$product->title}}" class="img-thumbnail img-fluid lazy" data-src="{{$product->image_url}}" />
                </a>
            </div>
            <div class="col-bottom" style="margin-top:15px;">
                <div class="rating text-center">
                    <span class="stars-rate">
                    <span class="stars-fill" style="width:84%;"></span>
                    </span>
                </div>
            </div>
            </div>

            <div class="col-xs-12 col-sm-7">
            <h2 class="title">
                <a href="{{$product->site_url}}" itemprop="url" class="ppc-provider-site" data-sid="1690" data-cid="140" data-source="click" >
                <span itemprop="name">{{$product->title}}</span>
                <i class="fa fa-external-link" style="font-size:17px;margin:0 0 0 10px;"></i>
                </a>
            </h2>
            <div class="excerpt">
            {{$product->description}}
            </div>
            <div class="col-bottom">
                <a href="#" id="software-more-info-4520" class="">
                Ver más información
                <i class="fa "></i>
                </a>
                |
                <a href="javascript:;" id="software-prices-4520" class="btn-form"
                    data-code="Zm9ybS1wcmljZXM="
                    data-param_catid="140"
                    data-param_sid="4520">
                Cotizar {{$product->title}}
                </a>
                |
                <a href="javascript:;" id="software-compare-4520" class="add-2-compare" 
                    data-id="4520" data-label="{{$product->title}}" data-href="http://dev.comparasoftware.com/platform/cmms-mp-version-10" 
                    data-image="/wp-content/uploads/2019/08/logomp-FB.png"
                    data-in='Comparando' data-out='+ Añadir a comparación'>
                + Añadir a comparación
                </a>
            </div>
            </div>
            <div class="col-xs-12 col-sm-2">
            <div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
                <a target="_blank" href="{{url($product->site_url)}}"  id="software-website-1690" class="btn btn-block ppc-provider-site"
                    data-sid="1690" data-cid="140" data-source="click">
                <i class="fa fa-external-link" style="font-size:25px;"></i>
                <span class="d-block">Ir a la web</span>
                </a>
            </div>
            </div>
        </div>
    </li>
    @endforeach 

</ul>