<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 <head>
    <link rel="icon" type="image/x-icon" href="{{URL::asset('images/favicon-150x150.png')}}" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CoreUI CSS -->
    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" crossorigin="anonymous">
    <link href="./node_modules/@coreui/chartjs/dist/css/coreui-chartjs.css" rel="stylesheet">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
 </head>
 <body >
    <div id="app" class="c-app">
        @guest
        @else
            <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
            <div class="c-sidebar-brand d-lg-down-none">
                <div class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo"> 
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="http://dev.comparasoftware.com/platform/images/logo-white.png" style="max-width:250px">
                        </a>
                </div>
                <div class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
                    <a class="navbar-brand" href="{{ url('/') }}">CS</a>
                </div>
            </div>
            <ul class="c-sidebar-nav">
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="/dashboard">
                    <svg class="c-sidebar-nav-icon">
                    <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-3d"></use>
                    </svg> 
                    Software</a>
                </li>
            </ul>
            <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
            </div>
        @endguest

        <div class="c-wrapper c-fixed-components">
            <header class="c-header c-header-light c-header-fixed c-header-with-subheader" style="background: #4FA6F1;">
                @guest
                <div class="c-sidebar-brand d-lg-down-none">
                    <div class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo"> 
                        <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="http://dev.comparasoftware.com/platform/images/logo-white.png" style="max-width:300px">
                        </a>
                    </div>
                </div>
                @endguest

                <ul class="c-header-nav ml-auto mr-4">
                    <!-- Authentication Links -->
                    @guest
                    <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>

                        <!-- Register -->
                        {{--  @if (Route::has('register'))
                            <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @endif 
                        --}}
                    @else

                    <li class="c-header-nav-item dropdown">
                        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <svg class="c-icon">
                            <use xlink:href="node_modules/@coreui/icons/sprites/free.svg#cil-people"></use>
                            </svg>
                            <div class="">&nbsp;{{ Auth::user()->name }}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pt-0">

                        <a class="dropdown-item" href="{{ route('logout') }}"     onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" >
                            <svg class="c-icon mr-2">
                            <use xlink:href="./node_modules/@coreui/icons/sprites/free.svg#cil-account-logout"></use>
                            </svg> {{ __('Logout') }}</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                        </div>
                    </li>
                    @endguest

                </ul>
                {{--  

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
                </ul>  --}}

            </header>

            <div class="c-body">
                <main class="c-main">
                    <div class="container-fluid">
                    <div class="fade-in">
                        @yield('content')

                    </div>
                    </div>

                </main>
            </div>

        </div>
    </div>



    <!-- CoreUI and necessary plugins-->
    <script src="./node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
    <!--[if IE]><!-->
    <script src="./node_modules/@coreui/icons/js/svgxuse.min.js"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    <script src="./node_modules/@coreui/chartjs/dist/js/coreui-chartjs.bundle.js"></script>
    <script src="./node_modules/@coreui/utils/dist/coreui-utils.js"></script>
    <script src="js/main.js"></script>

 </body>
</html>





{{--  <body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>  --}}
