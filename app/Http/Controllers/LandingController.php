<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class LandingController extends Controller
{
    /**
     * Show Home
     *
     * @return View
     */
    public function index()
    {
        $categories=Category::all();
        $products=Product::all();

        return view('landing.index',[
            'categories'=>$categories,
            'products'=>$products,
        ]);
    }
}
