<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Una categoria tiene uno a muchos features
     * @return [type] [description]
     */
    public function features() {
        return $this->hasMany('App\Feature','category_id');
    } 

}
