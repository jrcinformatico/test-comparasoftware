<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'feature_id'
    ];

    /**
     * Un productFeature pertenece a una producto
     * @return [type] [description]
     */
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    /**
     * Un productFeature pertenece a una feature
     * @return [type] [description]
     */
    public function feature()
    {
        return $this->belongsTo('App\Feature', 'feature_id');
    }

}
