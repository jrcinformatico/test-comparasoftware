<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description','image_url','site_url'
    ];


    /**
     * Un producto tiene uno a muchos productFeatures
     * @return [type] [description]
     */
    public function productFeatures() {
        return $this->hasMany('App\ProductFeature','product_id');
    } 

}
