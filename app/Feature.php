<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id'
    ];

    /**
     * Un feature pertenece a una categoria
     * @return [type] [description]
     */
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    /**
     * Un feature tiene uno a muchos productFeatures
     * @return [type] [description]
     */
    public function productFeatures() {
        return $this->hasMany('App\ProductFeature','feacture_id');
    } 
}
