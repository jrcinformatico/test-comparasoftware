<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entries = 
        [
            [
                "name" => "Lenguaje"
            ],
            [
                "name" => "Modelo de precios"
            ],
            [
                "name" => "Despliegue"
            ],
            [
                "name" => "Industrias"
            ],
            [
                "name" => "Paises"
            ],
            [
                "name" => "Características populares"
            ]
        ];


        foreach($entries as $entry){
            DB::table('categories')->insert($entry);
        }
    }

}
