<?php

use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entries = 
        [
            [
                "name" => "Español",
                "category_id" =>1 
            ],
            [
                "name" => "Ingles",
                "category_id" =>1 
            ],
            [
                "name" => "Portugues",
                "category_id" =>1 
            ],


            [
                "name" => "Prueba Gratuita",
                "category_id" =>2 
            ],
            [
                "name" => "Versión Gratuita",
                "category_id" =>2 
            ],
            [
                "name" => "Pago Mensual",
                "category_id" =>2 
            ],
            [
                "name" => "Pago anual",
                "category_id" =>2
            ],
            [
                "name" => "Pago de única vez ",
                "category_id" =>2 
            ],

            [
                "name" => "Nube, SaaS, Web",
                "category_id" =>3
            ],
            [
                "name" => "Instalado - Windows",
                "category_id" =>3 
            ],
            [
                "name" => "Instalado - Mac",
                "category_id" =>3 
            ],
            [
                "name" => "Instalado - Linux",
                "category_id" =>3 
            ],
            [
                "name" => "Dispositivo móvil - iOS Nativo",
                "category_id" =>3
            ],
            [
                "name" => "Dispositivo móvil - Android Nativo",
                "category_id" =>3 
            ],          


            [
                "name" => "Publicidad",
                "category_id" =>4 
            ],
            [
                "name" => "Agricultura",
                "category_id" =>4 
            ],
            [
                "name" => "Concesionario de automóviles",
                "category_id" =>4 
            ],
            [
                "name" => "Bancario",
                "category_id" =>4 
            ],
            [
                "name" => "Construcción",
                "category_id" =>4 
            ],
            [
                "name" => "Consultante",
                "category_id" =>4 
            ],
            [
                "name" => "Distribución",
                "category_id" =>4 
            ],
            [
                "name" => "Educación",
                "category_id" =>4 
            ],
            [
                "name" => "Energía",
                "category_id" =>4 
            ],
            [
                "name" => "Ingenieria",
                "category_id" =>4 
            ],
            [
                "name" => "Bebida alimenticia",
                "category_id" =>4 
            ],
            [
                "name" => "Salud / Medicina",
                "category_id" =>4 
            ],
            [
                "name" => "Hostelería / Viajes",
                "category_id" =>4 
            ],
            [
                "name" => "Seguro",
                "category_id" =>4 
            ],
            [
                "name" => "Legal",
                "category_id" =>4 
            ],
            [
                "name" => "Servicio de Mantenimiento / Campo",
                "category_id" =>4 
            ],
            [
                "name" => "Fabricación",
                "category_id" =>4 
            ],
            [
                "name" => "Medios de comunicación",
                "category_id" =>4 
            ],
            [
                "name" => "Hipoteca",
                "category_id" =>4 
            ],
            [
                "name" => "Sin ánimo de lucro",
                "category_id" =>4 
            ],
            [
                "name" => "Productos farmacéuticos",
                "category_id" =>4 
            ],
            [
                "name" => "Propiedad administrativa",
                "category_id" =>4 
            ],
            [
                "name" => "Sector público",
                "category_id" =>4 
            ],
            [
                "name" => "Bienes raíces",
                "category_id" =>4 
            ],
            [
                "name" => "Al por menor",
                "category_id" =>4 
            ],
            [
                "name" => "Software / TI",
                "category_id" =>4 
            ],
            [
                "name" => "Telecomunicaciones",
                "category_id" =>4 
            ],
            [
                "name" => "Transporte",
                "category_id" =>4 
            ],
            [
                "name" => "Utilidades",
                "category_id" =>4 
            ],
            [
                "name" => "Otros servicios",
                "category_id" =>4 
            ],


            [
                "name" => "México",
                "category_id" =>5 
            ],       
            [
                "name" => "España",
                "category_id" =>5 
            ],       
            [
                "name" => "Colombia",
                "category_id" =>5 
            ],       
            [
                "name" => "Chile",
                "category_id" =>5 
            ],       
            [
                "name" => "Argentina",
                "category_id" =>5 
            ],       
            [
                "name" => "Perú",
                "category_id" =>5 
            ],       
            [
                "name" => "Ecuador",
                "category_id" =>5 
            ],       
            [
                "name" => "Estados Unidos",
                "category_id" =>5 
            ],       
            [
                "name" => "Guatemala",
                "category_id" =>5 
            ],       
            [
                "name" => "Bolivia",
                "category_id" =>5 
            ],       
            [
                "name" => "Costa Rica",
                "category_id" =>5 
            ],       
            [
                "name" => "República Dominicana",
                "category_id" =>5 
            ],       
            [
                "name" => "Venezuela",
                "category_id" =>5 
            ],       
            [
                "name" => "Panama",
                "category_id" =>5 
            ],       
            [
                "name" => "El Salvador",
                "category_id" =>5 
            ],       
            [
                "name" => "Honduras",
                "category_id" =>5 
            ],       
            [
                "name" => "Uruguay",
                "category_id" =>5 
            ],       
            [
                "name" => "Paraguay",
                "category_id" =>5 
            ],       
            [
                "name" => "Puerto Rico",
                "category_id" =>5 
            ],       
            [
                "name" => "Brasil",
                "category_id" =>5 
            ],       
            [
                "name" => "Cuba",
                "category_id" =>5 
            ],

            [
                "name" => "Acceso Movil",
                "category_id" =>6
            ],
            [
                "name" => "Mantenimiento Preventivo",
                "category_id" =>6
            ],
            [
                "name" => "Mantenimiento Predictivo",
                "category_id" =>6
            ],
            [
                "name" => "Gestión en terreno",
                "category_id" =>6
            ]
        ];
        

    
        foreach($entries as $entry){
            DB::table('features')->insert($entry);
        }

    }
}
