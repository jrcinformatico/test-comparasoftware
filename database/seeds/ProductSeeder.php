<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $entries = 
        [
            [
                'title'=>'Fracttal', 
                'description'=>'Fracttal es la plataforma de gestión de activos empresariales para Internet de las cosas (IoT). Fracttal es una solución completa de software y hardware que combina comunicación de máquina a máquina (M2M), telemática y una poderosa tecnología de gestión de activos. Líder mundial con orígen en Latinoamérica.',
                'image_url'=>'https://www.fracttal.com/hubfs/fracttal-logo-header-2020.png',
                'site_url'=>'https://www.fracttal.com/es/'
            ],
            [
                'title'=>'CMMS MP versión 10', 
                'description'=>'Rediseñamos MP con una nueva interfaz mucho más intuitiva y funcional, bases de datos más robustas y nuevas herramientas que harán tu trabajo de gestión de mantenimi...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'eMaint CMMS', 
                'description'=>'eMaint CMMS ofrece plataformas innovadoras de fiabilidad de activos, que ayudarán a las organizaciones a aumentar su tiempo de actividad. Con una integración perfecta de herramientas de mantenimient...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'IBM Maximo', 
                'description'=>'IBM Maximo es la solución de gestión de activos empresariales líder a nivel mundial que le ayuda a optimizar su mantenimiento y obtener un mayor valor de sus activos. Gestione todos los tipos de ac...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'Neovero', 
                'description'=>'Software CMMS / EAM para gestión de Equipos Médicos ingeniería clínica / biomédica y mantenimiento hospitalario. Integrable en redes IoT para monitoreo de activos e Inteligencia Artificial. Neo...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'EasyMaint', 
                'description'=>'EasyMaint es un software especializado en la Gestión de Mantenimiento de Activos, diseñado para incrementar la vida útil de equipos industriales, equipos médicos, equipos de carga y todo tipo de m...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'WGM - Works Gestión de Mantenimiento', 
                'description'=>'Abismo-net es una aplicación web de gestión de mantenimiento, multiusuario, multiplanta, multi-almacenes y multilingüe de última generación. El objetivo principal de Abismo-net es realizar...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'TINC CMMS', 
                'description'=>'TINC CMMS ofrece funciones de gestión de inventarios, administración de la información de mantenimiento y estrategias para la evaluación del equipo médico al alcance de cualquier PC, Laptop o Tab...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'InnoMaint', 
                'description'=>'InnoMaint es un software de gestión de mantenimiento basado en la nube que supervisa, mide y gestiona todas sus actividades de mantenimiento asociadas con los equipos de las instalaciones...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ],
            [
                'title'=>'SoftMant', 
                'description'=>'Cada versión nueva que de SoftMant es para dar mejoras sustanciales al sistema y además para mantener compatibilidad con los nuevos sistemas operativos que salen cada año, también es important...',
                'image_url'=>$faker->imageUrl($width = 640, $height = 480),
                'site_url'=>$faker->domainName
            ]
        ];

        foreach($entries as $entry){
            DB::table('products')->insert($entry);
        }
    }
}
