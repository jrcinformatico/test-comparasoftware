<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();


        for ($i=0; $i <100 ; $i++) { 
            $entry=[   
                    'product_id'=>$faker->biasedNumberBetween($min = 1, $max = 10, $function = 'sqrt'),
                    'feature_id'=>$faker->biasedNumberBetween($min = 1, $max = 69, $function = 'sqrt')
                    ];

            DB::table('product_features')->insert($entry);
        }

    }
}
